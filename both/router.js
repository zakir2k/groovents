Router.configure({
  layoutTemplate: 'layout',
});

Router.route('/', {
  name: 'index',
  action: function(){
    if (this.ready()) {
      this.render('index');
    } else {
      this.render('loading');
    }
  },
});

Router.route('/events', {
  name: 'events',
  subscriptions: function(){
    return Meteor.subscribe('events');
  },
  action: function(){
    if (this.ready()) {
      this.render('events');
    } else {
      this.render('loading');
    }
  },
});

Router.route('/newEvent', {
  name: 'newEvent',
  action: function(){
    if (this.ready()) {
      this.render('newEvent');
    } else {
      this.render('loading');
    }
  },
});

Router.route('/newEvent/form', {
  name: 'newEventForm',
  action: function(){
    if (this.ready()) {
      this.render('newEventForm');
    } else {
      this.render('loading');
    }
  },
});

Router.route('event', {
  path: '/event/:_id',
  waitOn: function(){
    return Meteor.subscribe('event', this.params._id);
  }
});

Router.route('/profile', {
  name: 'profile',
  // waitOn: function(){
  //   return [IRLibLoader.load('http://localhost:3000/card/lib/js/card.js'), IRLibLoader.load("card.js")];
  // },
  action: function(){
    if (this.ready()) {
      this.render('profile');
    } else {
      this.render('loading');
    }
  },
});

Router.route('/receipt', {
  name: 'receipt',
  action: function(){
    if (this.ready()) {
      this.render('receipt');
    } else {
      this.render('loading');
    }
  },
});

Router.route('/receipt/:id', {
  name: 'receiptDetail',
  subscriptions: function(){
    return Meteor.subscribe('receipt-by-id', this.params.id);
  },
  action: function(){
    if (this.ready()) {
      this.render('receiptDetail');
    } else {
      this.render('loading');
    }
  },
});

Router.onBeforeAction('myLoggedInHook', {
  only: ['newEventForm']
});

Iron.Router.hooks.myLoggedInHook = function(){
  if (!Meteor.userId()) {
    // if the user is not logged in, render the Login template
    this.render('LoginMessage');
  } else {
    // otherwise don't hold up the rest of hooks or our route/action function
    // from running
    this.next();
  }
};