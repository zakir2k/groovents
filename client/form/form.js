Template.newEvent.created = function (){
  this.autorun(function(){
    Meteor.subscribe('events');
  }.bind(this));
};

Template.newEvent.events({
  'click #cancel': function(){
    Router.go('/');
  }
});


AutoForm.hooks({
  'events-new-form': {
    onSuccess: function (operation, result, template) {
      Meteor.call('publishTweet', result);
      Router.go('/events');
    },

    onError: function(operation, error, template) {
      alert(error);
    }
  }
});

Template.newEvent.rendered = function () {
  this.autorun(function () {
    if (!this.subscription.ready()) {
      IonLoading.show();
    } else {
      IonLoading.hide();
    }
  }.bind(this));
};