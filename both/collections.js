Events = new Mongo.Collection('events');
CreditCards = new Mongo.Collection('creditcards');
Receipts = new Mongo.Collection('receipts');

Events.before.insert(function (userId, doc) {
  console.log(userId);
  var array = [];
  array.push(userId);
  doc.user = userId;
  doc.username = Meteor.users.findOne({_id:userId}).profile.name;
  doc.attendees = array;
  doc.avatar = Meteor.users.findOne({_id:userId}).profile.avatar;
});

Events.attachSchema(new SimpleSchema({
  user: {
    type: String,
    optional: true,
    autoform: {
      omit: true
    }
  },
  username: {
    type: String,
    optional: true,
    autoform: {
      omit: true
    }
  },
  title: {
    type: Object
  },
  'title.main':{
    type: String,
      autoform: {
      'label-type': 'placeholder',
        placeholder: 'Event Title'
      }
  },
  price: {
    type: Object
  },
  'price.total':{
    type: Number,
      autoform: {
      'label-type': 'placeholder',
        placeholder: 'Total Event Price'
      }
  },
  date: {
    type: Object
  },
  'date.day':{
    type: Date,
    autoform: {
    'label-type': 'placeholder',
      placeholder: 'Event Date'
    }
  },
  'date.time':{
    type: String,
    autoform: {
    'label-type': 'placeholder',
      placeholder: 'Time'
    }
  },
  people: {
    type: Object
  },
  'people.total':{
    type: Number,
    autoform: {
    'label-type': 'placeholder',
      placeholder: '# People Required'
    }
  },
  'people.minimum':{
    type: Number,
    autoform: {
    'label-type': 'placeholder',
      placeholder: 'Min Number'
    }
  },
  details: {
    type: Object
  },
  'details.description': {
    type: String,
    label: 'Event Details',
    optional: true,
    autoform: {
      rows: 5,
      'label-type': 'stacked'
    }
  },
  location: {
    type: Object
  },
  'location.district': {
    type: String,
    max: 20
  },
  'location.address':{
    type: String,
    max: 200
  },
  avatar: {
    type: String,
    optional: true,
    autoform: {
      omit:true
    }
  },
  attendees: {
    type: [String],
    optional: true,
    autoform: {
      omit:true
    }
  }
}));