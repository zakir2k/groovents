Meteor.publish('events', function(){
  return Events.find();
});

Meteor.publish('event', function(id) {
  return Events.find({_id: id});
});

Meteor.publish('receipts', function(id) {
  return Receipts.find({user: id});
});

Meteor.publish('receipt-by-id', function(id) {
  return Receipts.find({_id: id});
});

