Template.event.created = function () {
  this.autorun(function () {
    var id = Router.current().params.id;
    this.subscription = Meteor.subscribe('event', id);
  }.bind(this));

  // We can use the `ready` callback to interact with the map API once the map is ready.
  GoogleMaps.ready('exampleMap', function(map) {
    // Add a marker to the map once it's ready
    var marker = new google.maps.Marker({
      position: map.options.center,
      map: map.instance
    });
  });
};

Template.event.rendered = function () {
  this.autorun(function () {
    if (!this.subscription.ready()) {
      IonLoading.show();
    } else {
      IonLoading.hide();
    }
  }.bind(this));
};

Template.event.helpers({
  event: function () {
    return Events.findOne();
  },
  currentURL: function(){
    return document.URL;
  },
  costpp: function(price, number){
    return Math.floor(price/number);
  },
  prettyDate: function(date){
    return date.toDateString();
  },
  user: function(eventId){
    var user = Meteor.userId;
    var evtArray = Events.findOne({_id:eventId}).attendees;
    if(evtArray.indexOf(user) !== -1){
      return true;
    }else{
      return false;
    }
  },
  exampleMapOptions: function() {
    // Make sure the maps API has loaded
    if (GoogleMaps.loaded()) {
      // Map initialization options
      return {
        center: new google.maps.LatLng(22.2832, 114.1470),
        zoom: 8
      };
    }
  }
});

Template.event.events({
  "click .join-button": function () {

    // Asynchronous call with a callback on the client
    var eventId = this._id;
    var amount = this.price.total / this.people.total;
    console.log("event and amount "+ eventId + "    " + amount);
    Meteor.call('transfer', amount, eventId, function (error, result) {
      if (error) {
        console.log("error");
      } else {
        console.log(result);
        // examine result
      }
      Router.go('/receipt');
    });
  }
});

// Template.payModal.events({

//   "click .submit-payment": function() {
//     console.log("running reciept");
//     Router.go('receipt');
//     $('.modal-backdrop').addClass("hide")

//   }
// })

var addUsertoEvent = function(eventId){
  var array = Events.findOne({_id:eventId}).attendees;
  array.push(Meteor.userId);
  Events.update({_id:eventId},{$set:{attendees:array}});
};


// ###############################

Template.payModal.rendered = function(){
    this.autorun(function () {
    if (!this.subscription.ready()) {
      IonLoading.show();
    } else {
      IonLoading.hide();
    }
  }.bind(this));
}


Template.payModal.created = function () {
  this.autorun(function () {
    var id = Router.current().params.id;
    this.subscription = Meteor.subscribe('event', id);
  }.bind(this));

  // We can use the `ready` callback to interact with the map API once the map is ready.
};

